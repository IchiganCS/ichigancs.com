---
layout: blog_type
title: Music
description: My opinions about music. The nature of it and what my personal preferences are.
backgroundimage: /images/main-background.jpg
backgroundimagedesc: Excerpt from Loewe's &quot;Der Woywode&quot; also known as &quot;Die Lauer&quot;
showinmore: true
language: en
---


One could spend entire days trying to grasp what music is or means. I'm still trying to do that although my knowledge is limited. This really is more to sort my thoughts and increase my understanding.

## What can music do?
Music can do all sorts of things - the most primitive kind of music is nothing more than nice sounds played together in a harmonic way. Like for example two drums, an octave apart, played alternatedly with a steady pace. This is music and it actually does nothing except sounding pleasantly. Music has a very easy side with no depth. This kind can also be understood by animals, because it is universal. There are objectively pleasant sounds, like for example the octave. It's double the frequency.

To better achieve this goal, you can develop music theory - it provides tools to easily achieve good sounding music: Music theory analyzes which sounds can be played in which context together and still sound nice. You can come up with names for specific intervals. For example you see that a sound accompanied by the third, the fifth sounds nice. You also find that not all natural sounds are nice: Parallel fifths sound bad.

There also is another, superior kind of music: When the music means something. When music wants to make the listener feel *something*, it admits that it wants to speak the emotional side of the human. This is a nobler goal and a huge improvement from the simple "sounds nice" stage.  To compose this kind of music, you need to extend the music theory we know. This is mainly done by experimenting: Which sounds played by which instruments make you feel certain things. This is, in most cases, what film music is. It uses advanced music theory and can be complex to compose/read/understand. Usually, film music is excellent at conjuring emotions. Not just film music, many popular genres are in this category too.

The last stage is when not the emotional side is targeted, but the human mind. We don't need more music theory - it is often wrongly claimed that "elitists" think that their music is superior because of music theory. Few actually do. I'll gladly admit that Jazz sometimes is more complex than classical music. Yet, it doesn't change a thing. Music theory is not, what makes classical music better. It is art. The two aforementioned stages both weren't art, they could at best be a craft. classical music is art. So, what actually is the difference between the two? Classical music shows the human what beauty is. The goal is not to make people feel things, but to make people see. Open their ears for something which does not exist in the human otherwise. Humans don't know beauty, they can only learn it from nature. And classical music is that: natural. Some may say that film music and pop music sound natural too: I disagree. This music sounds artificial, classical music doesn't.

This chapters name was "What can music do?" - it can sound nice, it can touch us emotionally and it can teach us beauty.

## What I can recommend
If you want to experience that beauty for yourself, you should go ahead and listen to one of my favorite composers: Bach. It is a great example because his music is seldom has any emotional value. That makes it easier to see what "beauty" his music possesses. Especially his fugues are a work of a master of music. In my opinion, it's not unjustifiable to say that he was the best composer ever.

But I, of course, think differently: Everyone who knows me, is aware of the truly best composer ever; *Carl Loewe*. As I'm writing this, I'm sitting in a car driving to the only museum on the world about him. What jovial times lie ahead. Carl Loewe is mainly known for his ballads which is fair. His other music is good too, but not as good. For those who can't imagine how a Loewe's ballads sound like, here is a prime example of [Josef Greindl](https://en.wikipedia.org/wiki/Josef_Greindl) (a German black bass and my favorite singer), accompanied by Michael Raucheisen, performing [Meerfahrt](https://youtu.be/6_3opptXQnE). This is a very classic ensemble: A piano and a singer. As you can hear, both follow the naturalness rule: Greindl never sounds weird like he did in later years (maybe his "r" is too much, but nevermind).
The crux of ballads is that they are not only musically interesting, but also lyrically. The text itself is art and the "musification" of it is even more so.

## Conclusion
Classical music is one of the few forms of beauty. Everyone who doesn't intuitively know what I mean by that, will most likely never understand. Read the poems and sing the songs of Schubert and Loewe, you will learn.