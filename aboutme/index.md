---
layout: blog_type
title: About me
description: I about myself - what I like and think about the world.
backgroundimage: /images/black-forest.jpg
backgroundimagedesc: Image of the black forest in Germany
showinmore: true
language: en
---

## Introduction
Hi! As of 2021, I'm 19 years old, German, male, and study Computer Science. 
My hobbies include, besides my passion for programming and computer science, undying love
for classical music, German poetry, and How To Train Your Dragon.
For those of you who are interested in my name, it is Samuel. 
I tell you because you can find it easily with one Google search.

As you may have noticed, I'd like to remain so anonymous that you don't know where I live. If this site were more than a
private project, then I would be required to add a legal notice, because of German laws. The full [ichigancs.com](http://ichigancs.com) domain is
a private project! Nothing more. Back to happier topics:

In my spare time, I either like to work on small projects like this site, listen to or perform
classical music, spend time with friends, or indulge in fantasy worlds. All of these are good fun, 
but some are more than that: Classical music and fantasy worlds both carry us to realms, describable as heavenly
and show us a little bit of truth in our painful (yet good) existence.

I'm adept at programming, an acceptable pianist, and an awful singer. Some may say I'm bad at these,
I'd argue that I have much room to improve. Improving not just in unimportant
things like coding in Java, but in understanding the world and true happiness.


## Music
Regarding music, I'm one of the worst elitists you can possibly find. My favorite genre is the 
[German lied](https://en.wikipedia.org/wiki/Lied) which is quite rare, even for classical music enthusiasts.
In that small circle of possible artists, I choose those who are dead. I firmly believe that with today's society the best singers
will not be able to achieve fame exceeding their home town. All good singers have died, but few of them have left a great legacy to be 
uncovered. Luckily, we can enjoy and learn from them, always hoping that someday we will see this kind of singing in person again.

I'm not nearly as educated about instrumental music as I'd like to. But I'm working on it.

Modern music is something I despise: There is no beauty to it, it's boring, dull, and without any meaning. It isn't art, it's only entertainment.
Don't get me wrong, this can be enough on some occasions - I won't deny that I listen to classical music for entertainment too. But if there is
nothing except superficial harmony, there is nothing that makes this music good. A good example is [The Sounds of Silence](https://youtu.be/l0q7MLPo-u8): It is a lovely song with meaning,
but [Disturbed's cover](https://youtu.be/u9Dg-g7t2l4) ruined it by destroying the intimacy of the piece.

Honorary mention: My favorite composer is [Carl Loewe](https://en.wikipedia.org/wiki/Carl_Loewe). He should be mentioned since I'm obsessed with him and his work.

## Chess
I'm moderate with chess too: I've been playing in the local chess club for a few years now - it's really nice to play a game or two with good friends.
You can find my profile on [chess.com](https://www.chess.com/member/ichigancs) or [lichess.org](https://lichess.org/@/IchiganCS).
I would describe myself as a positional player: I'm bad at it, but it's what I'm trying to improve at. At my level, tactics are of course more important,
but I don't play as tactical as I should.

## Religion, politics, and philosophy
I'm neither religious nor philosophically educated. Yet I won't deny that I like Christianity - they have solid values and know what is good.
They believe as much as I do that humans are superior to anything else. They do it because of their religious belief, I do it because I just believe it.
A few weeks ago, I found that it is not possible to rationally put a human above other animals, or even insects. Every attempt to deduce that fails without an initial
belief. Some may choose Reason as their indicator for how much a life is worth. But who gives Reason that power? It is a belief, nothing more.

From that point of view, I hate everything which kills humans as that is the worst thing you can do. I'm against the death penalty and abortion. Even
self-defense is morally horrible. Yet, that can be excused. Not forgiven, but excused. In Germany, you're not allowed to kill people, even in self-defense, but you won't be punished either.

I'm also politically conservative - using German parties, I'm somewhere between AfD and FDP. I wouldn't vote for the AfD, because they have too many stupid people
who are too important. The biggest flaw of the FDP is the lack of desired social normality. They think that there isn't a good way to live, that it is different for everybody. I don't think so. There can be a way in which all people can live together well. And that should be restored.

## Computer science

Currently, I'm developing an app for Android and Windows (a small app for DnD character management and a few utilities). 
Obviously this website and a game about HTTYD written with UnityEngine. My favorite language is C#. It combines style with static typing with 
accurate and supporting type inference. Furthermore, nothing beats good old C-style syntax. As it's part of the .NET-framework it has a lot of available
libraries and has great resources. It has overcome the flaws of older programming languages while not being too modern. A beautiful mix.
