---
layout: blog_type
title: More
description: Overview over all links
backgroundimage: /images/main-background.jpg
backgroundimagedesc: Excerpt from Loewe's &quot;Der Woywode&quot; also known as &quot;Die Lauer&quot;
specialcss: more.css
showinmore: false
language: en
---

For general questions please contact me via mail <contact@ichigancs.com>. 

The code for this website is [publicly available](https://gitlab.com/IchiganCS/ichigancs.com/).


## Blog entries
{% assign subpage = site.pages -%}
{%- for p in subpage -%}
{%- if p.showinmore %}
- {% if p.language == "de" -%}*German* - {% endif -%}[{{p.title}}]({{p.url}}): {{p.description}}

{% endif -%}
{%- endfor -%}



## Other platforms
<table>
    <tbody>
        {%- assign count = 0 -%}
        {%- for serv in site.data.accounts -%}
            {%- if serv.displaypublic -%}

                {%- if count == 0 -%}
                    <tr>
                    {%- assign count = 0 -%}
                {%- endif -%}

                {%- assign count = count | plus : 1 -%}

                <td>
                {%- if serv.link -%} 
                    <a href="{{serv.link}}">{{serv.service}}</a>: {{serv.name}} 
                {%- else -%} 
                    {{serv.service}}: {{serv.name}} 
                {%- endif -%}
                </td>

                {%- if count == 3 -%}
                    </tr>
                    {%- assign count = 0 -%}
                {%- endif -%}
            {%- endif -%}
        {%- endfor -%}
        {%- if count != 3 -%}
            </tr>
        {%- endif -%}
    </tbody>
</table>