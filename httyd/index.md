---
layout: blog_type
title: HTTYD
description: My opinions about HTTYD and what I will do about it.
backgroundimage: /images/httyd-background.jpg
backgroundimagedesc: Screenshot from the third movie
showinmore: true
language: en
---

## Why I love it
How To Train Your Dragon is great for several reasons:

- First and foremost: The story is good, makes sense and there aren't any "unsolvable" contradictions. Sure, a few small details don't add up, but all in all, it makes sense. it is realistic in its own way, we see another world that could theoretically exist. I can immerse myself in it.

- The characters are well-written and likable. Most things make sense. Especially Hiccup and Toothless, the crux of the movie. They behave in a relatable way, the emotions are perfectly perceivable.

- The music fulfills its responsibilities. It is not godly art but soaked with emotion in the best way imaginable. It's very good if not the best film music I have ever heard.
    
Those who have seen the movie may very well disagree with me on some points, maybe just partly. And rightly so. 
When I talk about HTTYD I only mean the first movie. A few other ones are ok (second movie and tv-show), other ones are bad (the third movie and the short movies).

## Why I don't like the other movies

The second one is ok. Valka has many, many internal contradictions, she doesn't fit into the beautifully crafted world of the first movie -
the creators obviously haven't thought of her when making it. She was later somehow pushed into the world without proper character development.
If she really had loved her son and missed him so much, she would have at least visited Berk from time to time. It's mainly this conflict that annoys me about Valka.

Another reason is the control over the Alpha which Drago undoubtedly has. It doesn't fit.
I don't care about the mind-control, that actually was a cool new thing. It is somehow unique and is greatly compatible with the first movie. 
I really liked that.

But how does Drago control the Alpha? With his weird shouting? No, that doesn't fit at all. The character of Drago is weird.

The third movie is much, much worse. One could use the following analogy: If the second movie was slightly sick, the third movie had total organ failure.
Every single character is worse. Everyone (except Gobber). The Light Fury has no context whatsoever. She just appears, Toothless falls in love with her and she somehow magically dislikes humans. What a convenience for the writers whose only goal it was to bring that ending which was in the film. They knew perfectly well that in a normal world, Toothless and Hiccup would never, never want to split up. So they created lots and lots of unfitting things to rip them apart and still couldn't do it. That's why they had to do another thing:

They changed the characters. Remember the introduction scene from the first movie: "We're Vikings. We have stubbornness issues."

Where was Hiccup's stubbornness when Toothless wanted to leave him? He just let him go. The reasoning makes sense, it was to protect Toothless. But why
couldn't he try to protect Toothless? Because the writers came up with an imaginary opponent whom they couldn't defeat. As if Hiccup would surrender
to an invisible enemy! Hiccup would try to fight like he always had done. He would do everything for Toothless, but in the movie he just let him go.

They changed the characters as they wanted, they introduced things without care for the world which already existed. They changed HTTYD to make the next movies.

## What now?
HTTYD is over. There never will be more official content. We can only enjoy what they have given us already. Apart from the amazing community of course.
There are a few wonderful people who produce content about HTTYD to this day and I'm thanking them all from the bottom of my heart.

There is only so much a single person can do, but I crave to contribute something myself. I don't know yet exactly what it will be: Either a fanfiction
or a computer game. I know there are people both willing and able to undertake such endeavors. But I have yet to find them. If there ever is a serious 
attempt on the latter, you will learn about it on the website of our [GitLab group](https://httyd-project.ichigancs.com/).
